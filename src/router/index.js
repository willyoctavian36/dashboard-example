import { createRouter, createWebHistory } from 'vue-router';
import Menu1 from '@/pages/Menu1.vue';
import Menu2 from '@/pages/Menu2.vue';

const routes = [
    {
        path: '/menu1',
        name: 'Menu1',
        component: Menu1
    },
    {
        path: '/menu2',
        name: 'Menu2',
        component: Menu2
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;